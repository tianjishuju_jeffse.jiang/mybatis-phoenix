package com.example.mybatisphoenix.service;



import com.example.mybatisphoenix.vo.ResultVO;

import java.util.List;
import java.util.Map;

/**
 * @autor kalista
 * @Date 2019/01/08
 */
public interface DataService {

    ResultVO add();

    ResultVO delete();

    ResultVO update();

    List<Map<String, Object>> query();

    int countDept();

}
