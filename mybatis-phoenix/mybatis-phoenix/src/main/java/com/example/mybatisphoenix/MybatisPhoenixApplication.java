package com.example.mybatisphoenix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @autor kalista
 * @Date 2019/01/08
 */
@SpringBootApplication
public class MybatisPhoenixApplication {

    private static final Logger log = LoggerFactory.getLogger(MybatisPhoenixApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MybatisPhoenixApplication.class, args);
        log.error("********************** SPRING BOOT UP SUCCESS *******************");
    }

}

